module.exports = {
  internal: {
    chat: {
      host: process.env.LISTEN_ADDRESS || "0.0.0.0",
    },
  },
};
